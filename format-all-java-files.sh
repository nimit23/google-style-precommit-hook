#!/usr/bin/env sh
mkdir -p .cache
cd .cache
echo "First Arg $1"
if [ ! -f google-java-format-1.7-all-deps.jar ]
then
    curl -LJO "https://github.com/google/google-java-format/releases/download/google-java-format-1.7/google-java-format-1.7-all-deps.jar"
    chmod 755 google-java-format-1.7-all-deps.jar
fi
cd ..

all_java_files=$(git ls-files "*.java" | grep ".*java$" )
echo $all_java_files
java -jar .cache/google-java-format-1.7-all-deps.jar --replace $all_java_files
